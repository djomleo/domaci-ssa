﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Model
{
    public class SubjectRepository
    {
        private string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

        public List<Subject> GetAllSubjects()
        {
            List<Subject> listToReturn = new List<Subject>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Subjects";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Subject s = new Subject();
                    s.Id = dataReader.GetInt32(0);
                    s.Name = dataReader.GetString(1);
                    listToReturn.Add(s);
                }
            }
            return listToReturn;
        }
    }
}
