﻿using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class StudentRepository
    {
        private string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

        public List<Student> GetAllStudents()
        {
            List<Student> listToReturn = new List<Student>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Students";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Student s = new Student();
                    s.Id = dataReader.GetInt32(0);
                    s.Name = dataReader.GetString(1);
                    s.Surname = dataReader.GetString(2);
                    s.Age = dataReader.GetInt32(3);
                    s.IndexNumber = dataReader.GetString(4);
                    listToReturn.Add(s);
                }
            }
            return listToReturn;
        }
    }
}