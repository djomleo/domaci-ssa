﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Model
{
    public class Mark
    {
        //promenljiva za smestanje podataka iz upita
        public string SubjectName { get; set; }
        public int MarkValue { get; set; }
        //public int StudentId { get; set; }
        //public int SubjectId { get; set; }
    }
}
