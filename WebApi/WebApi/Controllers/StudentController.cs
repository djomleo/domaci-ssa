﻿using BusinessLayer;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/student")]
    public class StudentController : ApiController
    {
        private StudentBusiness studentBusiness;

        public StudentController()
        {
            this.studentBusiness = new StudentBusiness();
        }
        [Route("getall")]
        [HttpGet]
        public List<Student> GetAllStudents()
        {
            return this.studentBusiness.GetAllStudents();
        }
    }
}