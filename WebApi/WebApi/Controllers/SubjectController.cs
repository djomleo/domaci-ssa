﻿using BusinessLayer;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/subject")]
    public class SubjectController : ApiController
    {
        private SubjectBusiness subjectBusiness;

        public SubjectController()
        {
            this.subjectBusiness = new SubjectBusiness();
        }
        [Route("getall")]
        [HttpGet]
        public List<Subject> GetAllSubjects()
        {
            return this.subjectBusiness.GetAllSubjects();
        }
    }
}