﻿using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class SubjectBusiness
    {
        private SubjectRepository subjectRepository;

        public SubjectBusiness()
        {
            this.subjectRepository = new SubjectRepository();
        }

        public List<Subject> GetAllSubjects()
        {
            List<Subject> subjects = this.subjectRepository.GetAllSubjects();
            if (subjects.Count > 0)
            {
                return subjects;
            }
            else
            {
                return null;
            }
        }
    }
}