﻿using DataLayer;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class StudentBusiness
    {
        private StudentRepository studentRepository;

        public StudentBusiness()
        {
            this.studentRepository = new StudentRepository();
        }
        public List<Student> GetAllStudents()
        {
            List<Student> students = this.studentRepository.GetAllStudents();
            if (students.Count > 0)
            {
                return students;
            }
            else
            {
                return null;
            }
        }
    }
}